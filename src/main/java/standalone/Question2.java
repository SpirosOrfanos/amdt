package standalone;

/**
 * .Create a function (digitSum) that accepts an integer and calculates the sum of it's digits.
 * If the sum is greater than 9 repeats the calculation of the sum of it's digits until we get sum < 10.
 * Returns the final sum.
 * examples
 * 10 -> 1 + 0 = 1 ... returns 1
 * 38 -> 3 + 8 = 11 -> 1 + 1 = 2 ... returns 2
 * 785 -> 7 + 9 + 5 = 21 -> 2 + 1 = 3  returns 3
 * 99892 -> 9 + 9 + 8 + 9 + 2 = 37 -> 3 + 7 = 10 -> 1 + 0 = 1 returns 1
 */
public class Question2 {
    public static void main(String... args) {
        Question2 app = new Question2();
        System.out.println(app.digitSum(0));
        System.out.println(app.digitSum(1));
        System.out.println(app.digitSum(10));
        System.out.println(app.digitSum(11));
        System.out.println(app.digitSum(100));
        System.out.println(app.digitSum(1001));
        System.out.println(app.digitSum(1234));
        System.out.println(app.digitSum(12340));
        System.out.println(app.digitSum(102340));
        System.out.println(app.digitSum(-1234));

    }

    private int digitSum(int num) {

        if (num < 0) num *= -1;
        int sum = 0;
        if (num < 10 || num == 0) return num;
        while (num > 0) {
            sum += num % 10;
            num /= 10;
        }


        return sum;
    }


}

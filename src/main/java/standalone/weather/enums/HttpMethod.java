package standalone.weather.enums;

public enum HttpMethod {
    GET("GET"),
    POST("POST");

    HttpMethod(String id) {
        this.id = id;
    }

    private String id;

    public HttpMethod getById(String id) {
        for (HttpMethod method : values()) {
            if (method.id.equalsIgnoreCase(id)) {
                return method;
            }
        }
        return null;
    }
}

package standalone.weather.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;
import java.util.Optional;

public class Util {


    public static String getParamsString(Map<String, String> params, boolean isBody) {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(entry.getKey()/*URLEncoder.encode(entry.getKey(), "UTF-8")*/);
            result.append("=");
            result.append(entry.getValue()/*URLEncoder.encode(entry.getValue(), "UTF-8")*/);
            result.append("&");
        }

        String resultString = result.toString();

        resultString = (resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString);
        return isBody ? resultString : "?" + resultString;
    }

    public static Double kelvinToCelsius(StringBuilder content) {
        content = new StringBuilder(content.substring(content.indexOf("temp")));
        return BigDecimal.valueOf(Double.valueOf(Double.valueOf(content.substring(0, content.indexOf(","))
                .substring(content.indexOf(":") + 1)) - 273))
                .round(MathContext.DECIMAL64)
                .setScale(1, BigDecimal.ROUND_HALF_UP)
                .doubleValue();

    }

    public static String parseAccessToken(String response) {
        return Optional.ofNullable(response)
                .map(item -> item.substring(item.indexOf(":") + 1, item.indexOf(",")))
                .map(item -> item.replaceAll("\"", ""))
                .orElse(null);
    }

    public static String template(String template, String... data) {
        String res = String.format(template, data);
        System.out.println(res);
        return res;
    }

}

package standalone.weather.task;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

public class WeatherTask extends GenericTask<String> {
    public WeatherTask(CompletableFuture c) {
        super(c);
    }
}

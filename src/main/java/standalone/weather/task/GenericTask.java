package standalone.weather.task;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

public abstract class GenericTask<T> implements Callable {

    CompletableFuture c;

    GenericTask(CompletableFuture c) {
        this.c = c;
    }
    @Override
    public Object call() throws Exception {
        return null;
    }
}

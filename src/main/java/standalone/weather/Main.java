package standalone.weather;

import standalone.weather.service.ApiWeatherService;
import standalone.weather.service.SMSTemplateService;
import standalone.weather.service.SMService;
import standalone.weather.service.WeatherService;

public class Main {
    public static void main(String... args) {

        Main app = new Main();
        app.start();
    }

    private void start() {
        (new WeatherService("Thessaloniki",
                new ApiWeatherService(),
                new SMService(new SMSTemplateService()))).run();
    }
}

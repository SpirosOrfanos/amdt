package standalone.weather.service;

import standalone.weather.task.WeatherTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WeatherService implements Runnable {
    private String location;
    private ApiWeatherService apiWeatherService;
    private SMService smService;
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
    private static final int MAX_ATTEMPTS = 1;

    public WeatherService(String location,
                          ApiWeatherService apiWeatherService,
                          SMService smService) {
        this.location = location;
        this.apiWeatherService = apiWeatherService;
        this.smService = smService;
    }

    @Override
    public void run() {
        System.out.println("Starting WeatherService for " + this.location);
        for (int i = 0; i < MAX_ATTEMPTS; i++) {
            try {
                System.out.println("Attempt " + i);

                scheduledExecutorService.schedule(
                        new WeatherTask(apiWeatherService.call(location)
                                .thenCompose(it -> smService.call(it))),
                        10,
                        TimeUnit.SECONDS).get();

            } catch (Exception e) {
                System.out.println("Failed to invoke location");
            } finally {

            }
        }
        scheduledExecutorService.shutdown();


    }
}

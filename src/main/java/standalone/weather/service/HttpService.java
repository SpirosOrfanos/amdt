package standalone.weather.service;

import standalone.weather.enums.HttpMethod;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public abstract class HttpService<T> {
    static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public HttpRequest buildRequest(String url,
                                    HttpMethod method,
                                    String[] headers,
                                    String body) throws Exception {
        System.out.println("URL " + url);

        switch (method) {
            case GET: {
                return HttpRequest.newBuilder()
                        .uri(new URI(url))
                        .headers(Optional.ofNullable(headers)
                                .orElse(new String[]{}))
                        .GET()
                        .build();
            }
            case POST: {
                return HttpRequest.newBuilder()
                        .uri(new URI(url))
                        .headers(Optional.ofNullable(headers)
                                .orElse(new String[]{}))
                        .POST(HttpRequest.BodyPublishers.ofString(body))
                        .build();
            }
            default:
                throw new UnsupportedOperationException("No such method exception");
        }

    }

    public abstract CompletableFuture call(T t);

    private void requestBodyTransform() {

    }
}

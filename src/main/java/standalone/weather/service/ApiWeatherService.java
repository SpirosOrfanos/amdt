package standalone.weather.service;

import standalone.weather.Main;
import standalone.weather.enums.HttpMethod;
import standalone.weather.util.Util;

import java.io.InputStream;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

public class ApiWeatherService extends HttpService<String> {

    private Properties properties = new Properties();

    public ApiWeatherService() {
        setUp();
    }

    private void setUp() {
        try (InputStream input = Main.class.getClassLoader().getResourceAsStream("config.properties")) {
            properties = new Properties();
            properties.load(input);
        } catch (Exception e) {

        }
    }

    public CompletableFuture<Double> call(String location) {
        System.out.println("WEATHER START");
        CompletableFuture<Double> cp = new CompletableFuture();

        try {
            HttpRequest request = buildRequest(properties.getProperty("url").concat(Util.getParamsString(
                    Map.of(
                            "q", location,
                            "appid", properties.getProperty("appid")), false)),

                    HttpMethod.GET,
                    new String[]{"Content-Type", "application/json", "Access-Control-Allow-Credentials", "true"},
                    null
            );

            httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> {
                        if (response.statusCode() > 200) cp.completeExceptionally(new Exception());
                        return response;
                    })
                    .thenApply(HttpResponse::body)
                    .thenAccept(content -> cp.complete(Util.kelvinToCelsius(new StringBuilder(content))));
            return cp;
        } catch (Exception e) {
            cp.completeExceptionally(new Exception());
            return cp;
        } finally {

        }

    }
}

package standalone.weather.service;

import standalone.weather.Main;
import standalone.weather.enums.HttpMethod;
import standalone.weather.util.Util;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

public class SMService extends HttpService<Double> {

    private Properties properties = new Properties();
    private SMSTemplateService smsTemplateService;

    public SMService(SMSTemplateService smsTemplateService) {
        this.smsTemplateService = smsTemplateService;
        setUp();
    }

    private void setUp() {
        try (InputStream input = Main.class.getClassLoader().getResourceAsStream("config.properties")) {
            properties = new Properties();
            properties.load(input);
        } catch (Exception e) {

        }
    }


    public CompletableFuture call(Double data) {
        CompletableFuture cp = new CompletableFuture();

        System.out.println("SMS START");

        try {
            HttpRequest request = buildRequest(properties.getProperty("sms.gw.url"),
                    HttpMethod.POST,
                    new String[]{
                            "content-type", properties.getProperty("sms.gw.content.type"),
                            properties.getProperty("sms.gw.header.auth"), properties.getProperty("sms.gw.header.auth.sec")},
                    Util.getParamsString(Map.of(properties.getProperty("sms.gw.grant.type"),
                            properties.getProperty("sms.gw.grant.type.type")), true));


            httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> {
                        if (response.statusCode() > 200) cp.completeExceptionally(new Exception());
                        return response;
                    })
                    .thenApply(HttpResponse::body)
                    .thenAccept(item ->  {
                        try {
                            httpClient.sendAsync(buildRequest(properties.getProperty("sms.gw.url"),
                                    HttpMethod.POST,
                                    new String[]{
                                            "content-type",
                                            properties.getProperty("sms.gw.content.type"),

                                            properties.getProperty("sms.gw.grant.sms.auth.type"),
                                            properties.getProperty("sms.gw.grant.sms.auth.sec").concat(Util.parseAccessToken(item)),

                                            "Expect",
                                            ""},
                                    (Util.template(
                                            smsTemplateService.smsBody(data, properties),
                                            ""+data,
                                            properties.getProperty("sms.gw.grant.sms.phone")))),
                                    HttpResponse.BodyHandlers.ofString())
                                    .thenApply(response -> {
                                        if (response.statusCode() > 200) cp.completeExceptionally(new Exception());
                                        return response;
                                    }).thenApply(HttpResponse::body)
                            .thenAccept(dat -> System.out.println(dat));
                        } catch (Exception e) {

                        }

                    });
            /* Util.parseAccessToken(item)*/


            cp.complete(data);
            return cp;


        } catch (Exception e) {
            cp.completeExceptionally(new Exception());
            return cp;
        } finally {
        }

    }

}

package standalone.weather.service.criteria;

public interface Criteria<T, E> {
    boolean check(T t, E e);
}

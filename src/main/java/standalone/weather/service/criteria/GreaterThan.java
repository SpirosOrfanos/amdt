package standalone.weather.service.criteria;

public class GreaterThan implements Criteria<Double, Double> {
    @Override
    public boolean check(Double aDouble, Double base) {
        return aDouble.compareTo(base) > 0;
    }
}

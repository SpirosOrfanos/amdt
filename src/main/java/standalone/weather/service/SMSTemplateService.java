package standalone.weather.service;

import standalone.weather.service.criteria.Criteria;
import standalone.weather.service.criteria.GreaterThan;

import java.util.Properties;

public class SMSTemplateService {
    private static final Double base = 20d;
    private Criteria criteria = new GreaterThan();

    public String smsBody(Double data, Properties properties) {
        if (criteria.check(data, base)) {
            return properties.getProperty("sms.gw.grant.sms.template_2");
        } else {
            return properties.getProperty("sms.gw.grant.sms.template_1");
        }
    }
}

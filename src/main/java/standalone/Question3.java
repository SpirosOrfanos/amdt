package standalone;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

/*
3.Create a function (doRemake) that takes a string of words and
-Move the first letter of each word to the end of the word.
-Add "ay" to the end of the word.
-Words starting with a vowel (a,e,i,o,u, A, E, I, O, U)
simply have "way" appended to the end.
-Be sure to preserve proper capitalization and punctuation.
Examples:
doRemake("Cats are great pets.")
returns "Atscay areway reatgay etspay."

doRemake("Tom got a small piece of pie.")
returns "Omtay otgay away allsmay iecepay ofway iepay."

doRemake("He told us a very exciting tale.")
returns "Ehay oldtay usway away eryvay excitingway aletay."
 */
public class Question3 {

    private static final String AY = "ay";
    private static final String WAY = "way";

    static Set<Integer> vowels =
            Set.of((int) 'a',
                    (int) 'e',
                    (int) 'i',
                    (int) 'o',
                    (int) 'u',
                    (int) 'A',
                    (int) 'E',
                    (int) 'I',
                    (int) 'O',
                    (int) 'U');

    public static void main(String... args) {
        Question3 app = new Question3();

        System.out.println(app.doRemake(""));
        System.out.println(app.doRemake(null));
        System.out.println(app.doRemake("a"));
        System.out.println(app.doRemake("A"));
        System.out.println("Cats are great pets");
        System.out.println(app.doRemake("Cats are great pets"));
        System.out.println("He told us a very exciting tale");
        System.out.println(app.doRemake("He told us a very exciting tale"));


    }


    private String doRemake(String data) {
        if (Objects.isNull(data)) return "";
        if (data.isEmpty()) return "";
        StringBuilder sb = new StringBuilder();

        Arrays.stream(data.split(" "))
                .forEach(item -> sb.append(subRemake(item)).append(" "));
        return capitalize(sb.toString());
    }

    private String subRemake(String data) {
        return customSupplier.apply(replaceSpecial.apply(data));
    }


    private Function<String, String> customSupplier =
            (data) ->
                    Objects.nonNull(data) && !data.isEmpty() ?
                            reverseAndApply.apply(data, vowels.contains((int) data.charAt(0)) ? WAY : AY) : "";


    private static BiFunction<String, String, String> reverseAndApply = (data, add) -> {
        StringBuilder sb = new StringBuilder();

        return sb.append(data.substring(1))
                .append(data.substring(0, 1).toLowerCase())
                .append(add).toString();
    };

    private static Function<String, String> replaceSpecial =
            (data) -> {
                if (Objects.isNull(data)) return null;
                data = data.trim();
                StringBuilder sb = new StringBuilder();
                for (char c : data.trim().toCharArray()) {
                    if (c < 123 && c >= 64) {
                        sb.append(c);
                    }
                }
                return sb.toString().length() > 0 ? sb.toString() : "";
            };

    public static String capitalize(final String str) {
        final int strLen = str.length();
        if (strLen == 0) {
            return str;
        }

        final int firstCodepoint = str.codePointAt(0);
        final int newCodePoint = Character.toTitleCase(firstCodepoint);
        if (firstCodepoint == newCodePoint) {
            // already capitalized
            return str;
        }

        final int[] newCodePoints = new int[strLen]; // cannot be longer than the char array
        int outOffset = 0;
        newCodePoints[outOffset++] = newCodePoint; // copy the first codepoint
        for (int inOffset = Character.charCount(firstCodepoint); inOffset < strLen; ) {
            final int codepoint = str.codePointAt(inOffset);
            newCodePoints[outOffset++] = codepoint; // copy the remaining ones
            inOffset += Character.charCount(codepoint);
        }
        return new String(newCodePoints, 0, outOffset);
    }
}

package standalone;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiFunction;

/*
1. Create a function (findSeven) that takes an array of numbers and return "Found"
if the character 7 appears in the array of the numbers. Otherwise, return "there is no 7 in the array".
Examples :
findSeven([1, 2, 3, 4, 5, 6, 7]) ➞ "Found
findSeven([8, 6, 33, 100]) ➞ "there is no 7 in the array"
findSeven([2, 55, 60, 97, 86]) ➞ "Found"
 */
public class Question1 {
    public static void main(String... args) {
        Question1 app = new Question1();
        System.out.println(app.findSeven(null));
        System.out.println(app.findSeven(new int[]{}));
        System.out.println(app.findSeven(new int[]{2}));
        System.out.println(app.findSeven(new int[]{7}));
        System.out.println(app.findSeven(new int[]{1, 2, 3, 4}));
        System.out.println(app.findSeven(new int[]{1, 2, 3, 4, 7}));
        System.out.println(app.findSeven(new int[]{1, 2, 3, 4, 7, 8}));
        System.out.println(app.findSeven(new int[]{1, 2, 37, 4, 2, 8}));
        System.out.println(app.findSeven(new int[]{1, 2, 376, 4, 2, 8}));


    }

    private String findSeven(int[] arr) {
        if (Objects.isNull(arr) || arr.length == 0) return "there is no 7 in the array";
        return Arrays.stream(arr).anyMatch(i -> containsDigit.apply(i, 7).booleanValue()) ? "Found" : "there is no 7 in the array";

    }

    private BiFunction<Integer, Integer, Boolean> containsDigit =
            (num, digit) -> {
                if (num < 0) num *= -1;
                while (num > 0) {
                    if (num % 10 == digit) return true;
                    num /= 10;
                }
                return false;
            };
}
